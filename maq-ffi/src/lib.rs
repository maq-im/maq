use ::safer_ffi::prelude::*;

use maq_interop::reqrep::{ReceivedInfo, Rep, RepEntry, Req, ReqEntry};

use std::sync::{Arc, Mutex};

use prost::Message;

#[::safer_ffi::cfg_headers]
#[test]
fn generate_headers() -> ::std::io::Result<()> {
    ::safer_ffi::headers::builder()
        .with_guard("__MAQ__")
        .to_file("maq.h")?
        .generate()
}

#[test]
fn test_encode() {
    // let req = Req::new(ReqEntry::SendUserIdentifier(send_user_identifier::Req {
    //     user_identifier: "bhat".to_string(),
    // }));
    //
    // let b = req.encode();
    //
    // let _req = Req::decode(b).unwrap();
}

#[derive_ReprC]
#[ReprC::opaque]
pub struct Client {
    rt: Arc<Mutex<tokio::runtime::Runtime>>,
    c: Arc<maq::client::Client>,

    tx: crossbeam_channel::Sender<bytes::Bytes>,
    rx: crossbeam_channel::Receiver<bytes::Bytes>,
}

#[ffi_export]
fn new_client(
    homeserver_url: char_p::Ref<'_>,
    db_dir: char_p::Ref<'_>,
    file_dir: char_p::Ref<'_>,
) -> repr_c::Box<Client> {
    let homeserver_url = url::Url::parse(homeserver_url.to_str()).unwrap();
    let db_dir = db_dir.to_str();
    let file_dir = file_dir.to_str();

    let c = maq::client::Client::new(homeserver_url, db_dir, file_dir).unwrap();

    let (tx, rx) = crossbeam_channel::unbounded::<bytes::Bytes>();

    let client = Client {
        c: Arc::new(c),
        rt: Arc::new(Mutex::new(tokio::runtime::Runtime::new().unwrap())),

        tx,
        rx,
    };

    repr_c::Box::new(client)
}

#[ffi_export]
fn client_send_req(client: &mut Client, req: c_slice::Ref<'_, u8>) {
    let rt = client.rt.lock().unwrap();
    let req = Req::decode(req.as_slice()).unwrap();

    if let Some(entry) = req.entry {
        rt.spawn(handle_req(
            client.c.clone(),
            client.tx.clone(),
            entry,
            req.extra,
        ));
    }
}

async fn handle_req(
    client: Arc<maq::client::Client>,
    tx: crossbeam_channel::Sender<bytes::Bytes>,
    req_entry: ReqEntry,
    extra: String,
) {
    match req_entry {
        ReqEntry::AuthorizationStatus(_) => {
            use maq_interop::reqrep::authorization_status;

            let auth_status = client.auth_status().await;

            let msg = ReceivedInfo::from_rep(Rep::new(
                RepEntry::AuthorizationStatus(authorization_status::Rep {
                    status: auth_status as i32,
                }),
                extra,
            ))
            .to_proto();

            tx.send(msg).unwrap();
        }
        ReqEntry::Authorize(_) => {
            use maq_interop::reqrep::authorize;

            client.authorize().await.unwrap();

            let msg =
                ReceivedInfo::from_rep(Rep::new(RepEntry::Authorize(authorize::Rep {}), extra))
                    .to_proto();

            tx.send(msg).unwrap();
        }
        ReqEntry::SendUserIdentifier(req) => {
            use maq_interop::reqrep::send_user_identifier;

            client
                .send_user_identifier(req.user_identifier)
                .await
                .unwrap();

            let msg =
                ReceivedInfo::from_rep(Rep::new(RepEntry::SendUserIdentifier(send_user_identifier::Rep {}), extra))
                    .to_proto();

            tx.send(msg).unwrap();
        }
        ReqEntry::SendPassword(req) => {
            use maq_interop::reqrep::send_password;

            client.send_password(req.password).await.unwrap();

            let msg =
                ReceivedInfo::from_rep(Rep::new(RepEntry::SendPassword(send_password::Rep {}), extra))
                    .to_proto();

            tx.send(msg).unwrap();
        }
    };
}

#[ffi_export]
fn client_recv_rep(client: &mut Client, timeout: u32) -> c_slice::Box<u8> {
    let msg = client.rx.recv().unwrap();

    let msg_bytes = msg.iter().cloned().collect::<Vec<u8>>();
    msg_bytes.into_boxed_slice().into()
}

#[ffi_export]
fn client_destroy(client: repr_c::Box<Client>) {
    drop(client)
}

#[ffi_export]
fn bytes_destroy(slice: c_slice::Box<u8>) {
    drop(slice);
}
