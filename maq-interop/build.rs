extern crate prost_build;

fn main() {
    let mut config = prost_build::Config::default();
    config.out_dir("src/interop");

    config
        .compile_protos(
            &[
                "interop/base.proto",
                "interop/auth.proto",
                "interop/reqrep.proto",
                "interop/update.proto",
            ],
            &["interop/"],
        )
        .unwrap();
}
