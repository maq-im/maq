#[derive(Clone, PartialEq, ::prost::Message)]
pub struct Update {
    #[prost(oneof="update::Entry", tags="1")]
    pub entry: ::std::option::Option<update::Entry>,
}
pub mod update {
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct NewMessage {
        #[prost(string, tag="10")]
        pub content: std::string::String,
    }
    #[derive(Clone, PartialEq, ::prost::Oneof)]
    pub enum Entry {
        #[prost(message, tag="1")]
        NewMessage(NewMessage),
    }
}
