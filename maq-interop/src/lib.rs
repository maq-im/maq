mod interop;
pub use interop::*;

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        use crate::base::MxcUri;

        let url = MxcUri {
            server_name: "Ok".to_string(),
            file_id: "ID".to_string(),
        };
    }
}
