use serde::{Deserialize, Serialize};

use std::convert::TryFrom;

#[derive(Serialize, Deserialize)]
pub struct Session {
    user_id: String,
    access_token: String,
    device_id: String,
}

impl From<Session> for matrix_sdk::Session {
    fn from(session: Session) -> Self {
        matrix_sdk::Session {
            access_token: session.access_token,
            user_id: matrix_sdk::identifiers::UserId::try_from(session.user_id.as_str()).unwrap(),
            device_id: matrix_sdk::identifiers::DeviceIdBox::from(session.device_id),
        }
    }
}

impl From<matrix_sdk::Session> for Session {
    fn from(session: matrix_sdk::Session) -> Self {
        Session {
            user_id: session.user_id.to_string(),
            access_token: session.access_token,
            device_id: session.device_id.to_string(),
        }
    }
}
